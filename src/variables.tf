variable tf_state_bucket {
    type = string
    description = "Bucket used to store TF state"
}

variable "project" {
    type = string
    description = "The project ID for the app"
}

variable "builder" {
    type = string
    description = "The email for the SA that builds this repo"
}