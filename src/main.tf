locals {
    location = "us-central1"
    loader_image = "data-loader"
}

resource "google_artifact_registry_repository" "primary" {
    provider = google-beta

    project = var.project
    location = local.location
    repository_id = ""
    format = "DOCKER"
}