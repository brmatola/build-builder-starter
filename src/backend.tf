terraform {
    backend "gcs" {
        bucket = var.tf_state_bucket
    }
}

terraform {
    required_version = "~> 1.0.0"
}

provider "google" {
  project = "${var.project}"
  region = "us-central1"
}