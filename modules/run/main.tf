resource "google_service_account" "runner" {
    account_id = var.sa_config.id
    display_name = var.sa_config.display_name
}

# SA setting this up needs access rights
resource "google_service_account_iam_binding" "impersonator" {
    service_account_id = google_service_account.runner.name
    role = "roles/iam.serviceAccountUser"
    members = ["serviceAccount:${var.impersonator_sa_email}"]
}

resource "google_cloud_run_service" "service" {
    name = var.name
    location = var.location

    template {
        spec {
            service_account_name = google_service_account.runner.email
            containers {
                image = "${var.docker_image.name}:${var.docker_image.tag}"
            }
        }
    }

    traffic {
        percent = 100
        latest_revision = true
    }

    lifecycle {
        ignore_changes = [
            metadata.0.annotations
        ]
    }

    depends_on = [ google_service_account_iam_binding.impersonator ]
}

resource "google_cloud_run_service_iam_binding" "invoker" {
    location = var.location
    service = google_cloud_run_service.service.name
    role = "roles/run.invoker"
    members = ["serviceAccount:${var.invoker_sa_email}"]
}

# SA running build needs access to resource and SA running
resource "google_cloud_run_service_iam_binding" "builder" {
    location = var.location
    service = google_cloud_run_service.service.name
    role = "projects/covid-dashboard-339116/roles/cloudRunner"
    members = ["serviceAccount:${var.builder_sa_email}"]
}

resource "google_service_account_iam_binding" "runner_impersonator" {
    service_account_id = google_service_account.runner.name
    role = "roles/iam.serviceAccountUser"
    members = ["serviceAccount:${var.builder_sa_email}"]
}
