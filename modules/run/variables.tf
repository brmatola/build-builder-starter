variable "name" {
    type = string
}

variable "location" {
    type = string
}

variable "impersonator_sa_email" {
    type = string
}

variable "builder_sa_email" {
    type = string
}

variable "invoker_sa_email" {
    type = string
}

variable "docker_image" {
    type = object({
        name = string
        tag = string
    })
}

variable "sa_config" {
    type = object({
        id = string
        display_name = string
    })
}