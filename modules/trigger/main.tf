locals {
  trigger_audience = "data-loader"
}

resource "google_service_account" "trigger" {
    account_id = var.sa_config.id
    display_name = var.sa_config.display_name
}

resource "google_cloud_scheduler_job" "job" {
  name = "scheduler-trigger"
  schedule = "*/8 * * * *"
  attempt_deadline = "320s"

  http_target {
    http_method = "POST"
    uri = var.push_endpoint

    oidc_token {
      service_account_email = google_service_account.trigger.email
      audience = var.url
    }
  }

  depends_on = [google_service_account_iam_binding.trigger_impersonator]
}

resource "google_service_account_iam_binding" "trigger_impersonator" {
    service_account_id = google_service_account.trigger.name
    role = "roles/iam.serviceAccountUser"
    members = ["serviceAccount:${var.builder_sa_email}"]
}