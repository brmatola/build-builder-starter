variable "push_endpoint" {
    type = string
}

variable "url" {
    type = string
}

variable "sa_config" {
    type = object({
        id = string
        display_name = string
    })
}

variable "builder_sa_email" {
    type = string
}