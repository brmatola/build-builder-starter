variable "project" {
    type = string
}

variable "location" {
    type = string
}

variable "impersonator_sa_email" {
    type = string
}

variable "cloud_run_name" {
    type = string
}

variable "docker_config" {
    type = object({
        repo_id = string
        repo_name = string
        image_name = string
    })
}

variable "build_config" {
    type = object({
        repo_name = string
        log_bucket_name = string
        trigger_name = string
        trigger_description = string
    })
}

variable "sa_config" {
    type = object({
        id = string
        display_name = string
    })
}