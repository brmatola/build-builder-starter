resource "google_service_account" "builder" {
    account_id = var.sa_config.id
    display_name = var.sa_config.display_name
}

# do git pull
resource "google_sourcerepo_repository_iam_binding" "builder" {
    project = var.project
    repository = var.build_config.repo_name
    role = "roles/source.reader"
    members = ["serviceAccount:${google_service_account.builder.email}"]
}

# read and write docker images to registry
resource "google_artifact_registry_repository_iam_binding" "builder" {
  provider = google-beta
  project = var.project
  location = var.location
  repository = var.docker_config.repo_id
  role = "roles/artifactregistry.writer"
  members = ["serviceAccount:${google_service_account.builder.email}"]
}

# SA setting this up needs access rights
resource "google_service_account_iam_binding" "impersonator" {
    service_account_id = google_service_account.builder.name
    role = "roles/iam.serviceAccountUser"
    members = ["serviceAccount:${var.impersonator_sa_email}"]
}

data "google_sourcerepo_repository" "infrastructure" {
    name = var.build_config.repo_name
}

resource "google_cloudbuild_trigger" "infra_trigger" {
    name = var.build_config.trigger_name
    description = var.build_config.trigger_description
    service_account = "projects/${var.project}/serviceAccounts/${google_service_account.builder.email}"
    filename = "cloudbuild.yaml"

    trigger_template {
      branch_name = "main"
      repo_name = data.google_sourcerepo_repository.infrastructure.name
    }

    substitutions = {
        _LOG_BUCKET_URL = google_storage_bucket.build_logs.url
        _DEV_IMAGE_NAME = "${var.docker_config.repo_name}/${var.docker_config.image_name}"
        _CLOUD_RUN_NAME = var.cloud_run_name
    }

    depends_on = [ google_service_account_iam_binding.impersonator ]
}

resource "google_storage_bucket" "build_logs" {
    name = "${var.project}-${var.build_config.log_bucket_name}-build-logs"
    location = "US"
}

resource "google_storage_bucket_iam_binding" "admin" {
    bucket = google_storage_bucket.build_logs.name
    role = "roles/storage.admin"
    members = ["serviceAccount:${google_service_account.builder.email}"]
}